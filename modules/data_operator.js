const db = require('../models');
console.log(typeof db.users.findByPk);

const insertData = async (data, table) => {
  return db[table].create(data)
    .then((created) => {
      const result = created.get({ plain: true })
      return result;
    }).catch((err) => {
      throw err;
    });
}
const findByPk = async (id, table) => {
  return db[table].findByPk(id)
    .then(async (found) => {
      if (found) {
        const result = await found.get({ plain: true });
        return result;
      }
      return null;
    }).catch((err) => {
      throw err;
    });
}
const findOne = async (data, table) => {
  return db[table].findOne(data)
    .then(async (found) => {
      if(found) {
        const result = await found.get({ plain: true });
        return result;
      }
      return null;
    }).catch((err) => {
      throw err;
    });
}
const findAll = async (data, table) => {
  return db[table].findAll(data)
    .then(async (found) => {
      const result = await found.map(async (item) => {
        return await item.get({ plain: true });
      });
      return result;
    }).catch((err) => {
      throw err;
    });
}
const updateData = async (id, data, table) => {
  return db[table]
    .update(data, { where: { id: id } })
    .then((deleted) => {
      return deleted;
    }).catch((err) => {
      throw err;
    });;
}
const deleteData = async (id, table) => {
  return db[table].destroy({ where: { id: id } })
    .then((deleted) => {
      return deleted;
    }).catch((err) => {
      throw err;
    });
}
const paginationData = (page_number, page_size, count) => {
  const pageNumber = parseInt(page_number);
  const pageSize = parseInt(page_size);
  const totalPage = Math.ceil(count / pageSize);
  const pagination = {
    current_page: pageNumber,
    page_size: pageSize,
    total_page: totalPage,
    first_page: 1,
    next_page: totalPage > pageNumber ? (pageNumber + 1) : pageNumber,
    prev_page: 1 < pageNumber ? (pageNumber - 1) : pageNumber,
  };
  return pagination;
}

//export to application use
module.exports = {
  insertData,
  findByPk,
  findOne,
  findAll,
  updateData,
  deleteData,
  paginationData,
}