const uppercase = (text) => {
  return text.toUpperCase();
};
const lowercase = (text) => {
  return text.toLowerCase();
};
const capitalize = (text) => {
  return text.charAt(0).toUpperCase() + text.slice(1).toLowerCase();
};
const capitalizeAll = (text) => {
  return text.toLowerCase().split(' ').map(word => word.charAt(0).toUpperCase() + word.slice(1).toLowerCase()).join(' ');
};
const reverse = (text) => {
  return text.split('').reverse().join('');
};
const removeSpaces = (text) => {
  return text.replace(/\s/g, '');
};
const encrypt = (text) => {
  return text.split('').map(char => char.charCodeAt(0)).join(' ');
};
const decrypt = (text) => {
  return text.split(' ').map(char => String.fromCharCode(char)).join('');
};
const getBitFromChar = (text) => {
  return text.split('').map(char => char.charCodeAt(0).toString(2)).join(' ');
};
const getCharFromBit = (text) => {
  return text.split(' ').map(char => String.fromCharCode(parseInt(char, 2))).join('');
};
const clear = (text) => {
  return text.replace(/\/\w+\s?/g, '');
};

module.exports = {
  uppercase,
  lowercase,
  capitalize,
  capitalizeAll,
  reverse,
  removeSpaces,
  encrypt,
  decrypt,
  getBitFromChar,
  getCharFromBit,
  clear
};