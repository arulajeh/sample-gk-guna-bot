const clearCommandTag = (ctx) => {
    ctx.message.text = ctx.message.text.replace(/\/\w+\s?/g, '');
    return ctx;
};