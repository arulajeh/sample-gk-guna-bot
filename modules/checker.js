const listOfBadWordsIndonesian = [
	"anjing",
	"goblok",
	"babi",
	"asu",
	"sialan",
	"bangsat",
	"bajingan",
	"kampret",
	"kontol",
	"sampah",
	"sialan",
	"tai",
	"banci",
	"maho",
	"pelacur"
];

const isContainsBadWords = (text) => {
	return listOfBadWordsIndonesian.some(word => text.toLowerCase().includes(word));
};

module.exports = {
	isContainsBadWords
}