const express = require('express');
const app = express();
const compression = require('compression');
const axios = require('axios').default;
const { Telegraf, Markup } = require('telegraf');
const { createClient } = require('redis');
const { isContainsBadWords } = require('./modules/checker');
const { findOne, findByPk, insertData, updateData } = require('./modules/data_operator');

app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ limit: '50mb', extended: true }));

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'DELETE, PUT');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, X-Token");
  next();
});
app.use(
  compression({
    level: 9,
    memLevel: 9,
    strategy: 'gzip',
    threshold: '1kb',
    flush: 'sync',
    chunkSize: '1kb',
    windowBits: 9
  })
);

app.use('/files', express.static('public', {
  maxAge: 3600 * 1000,
  immutable: true
}));

const redisClient = createClient({
  // password: "Bisajadi100", 
  url: "redis://47.241.93.56:6379"
});
redisClient.connect().then(() => { console.log('redis connected') });

app.get('/', (req, res) => {
  res.json({
    message: 'Welcome to Gak Guna Bot API'
  });
});

app.get('/genres', async (req, res) => {
  const genresdata = await redisClient.get("genres");
  if (genresdata) {
    return res.json({ genres: JSON.parse(genresdata), message: "from cache" });
  }
  const genres = await axios("https://api.themoviedb.org/3/genre/movie/list?api_key=818813472a9cd67f5872135a7ddef715");
  await redisClient.setEx("genres", 3600, JSON.stringify(genres.data.genres));
  res.json({
    genres: genres.data.genres
  })
});

// const bot = new Telegraf(process.env.BOT_TOKEN || "5237033621:AAESVexvLaiUGAPy550owbej-_Kx8cTgIiw");
// bot.middleware((ctx, next) => {
//   if (isContainsBadWords(ctx.message.text)) {
//     ctx.reply('Gak Boleh Berkata Kasar Ya DEKK!');
//   } else {
//     next();
//   }
// });
// bot.command('start', async ctx => {
//   const user = await findByPk(ctx.from.id, 'users');
//   if (user) {
//     redisClient.set(ctx.from.id + '_state', user.state);
//     ctx.reply('Welcome back bro ' + ctx.from.first_name + '!');
//   } else {
//     redisClient.set(ctx.from.id + '_state', "start");
//     insertData(ctx.from, 'users');
//     await ctx.reply('Welcome bro ' + ctx.from.first_name);
//     ctx.reply('Kamu orang mana bro? Coba shareloc dong!');
//   }
// });

// bot.on('location', async ctx => {
//   const state = await redisClient.get(ctx.from.id + '_state');
//   if (state == "start") {
//     redisClient.set(ctx.from.id + '_state', "location");
//     const data = {
//       latitude: ctx.message.location.latitude,
//       longitude: ctx.message.location.longitude,
//       userid: ctx.from.id
//     }
//     insertData(data, 'location');
//     ctx.reply('Oke locationnya gue simpen ya bro!');
//   } else {
//     ctx.reply('Kamu udah pernah share location kok bro!');
//   }
// });

// bot.on('text', async ctx => {
//   const state = await redisClient.get(ctx.from.id + '_state');
//   console.log(state)
//   switch (state) {
//     case 'start':
//       ctx.reply('Kamu belum kirim location nih, kirim dulu dong');
//       break;
//     case 'location':
//       ctx.reply('Deket nih rumahnya sama admin ternyata');
//       ctx.reply('One time keyboard', Markup
//         .keyboard([
//           ['🔍 Search', '😎 Popular'], // Row1 with 2 buttons
//           ['☸ Setting', '📞 Feedback'], // Row2 with 2 buttons
//           ['📢 Ads', '⭐️ Rate us', '👥 Share'] // Row3 with 3 buttons
//           // {
//           //   callback_data: "test",
//           //   text: "test"
//           // }
//         ])
//         .oneTime()
//         .resize()
//       )
//       break;
//     default:
//       ctx.reply('Silahkan klik /start ya bro');
//       break;
//   }
// });

// bot.on('callback_query', async ctx => {
//   console.log(ctx.callbackQuery.data)
//   ctx.answerCbQuery('Ok bro');
//   Markup.removeKeyboard();
//   // ctx.editMessageText('Ok bro');
//   // ctx.replyWithLocation(3.14, 3.14);
// });

// bot.command('finish', ctx => {
//   ctx.reply('Goodbye cok!');
// });

// bot.launch();

// Enable graceful stop
// process.once('SIGINT', () => bot.stop('SIGINT'));
// process.once('SIGTERM', () => bot.stop('SIGTERM'));


app.listen(process.env.PORT || 3000, () => {
  console.log('Server started on port 3000');
});