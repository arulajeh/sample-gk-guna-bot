const fs = require('fs');
const config = require('../config/config');
const Sequelize = require("sequelize");
const sequelize = new Sequelize(config.database, config.username, config.password, {
    host: config.host,
    dialect: config.dialect,
    logging: false,
});
const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;
// get file name list using fs
const files = fs.readdirSync(__dirname);
// loop through files and require each one
files.forEach(function (file) {
    if (file !== 'index.js') {
        const modelName = file.split('.')[0];
        const model = require(__dirname + '/' + modelName)(sequelize, Sequelize);
        db[modelName] = model;
    }
});

db.users.hasOne(db.location, { foreignKey: 'userid' });
db.users.hasMany(db.chat_histories, { foreignKey: 'userid' });
db.location.belongsTo(db.users, { foreignKey: 'userid' });
db.chat_histories.belongsTo(db.users, { foreignKey: 'userid' });

// export the database object for the application to use
module.exports = db;