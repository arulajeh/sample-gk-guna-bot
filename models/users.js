module.exports = (sequelize, Sequelize) => {
    const Users = sequelize.define("users", {
        id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            primaryKey: true
        },
        first_name: {
            type: Sequelize.STRING,
            trim: true
        },
        last_name: {
            type: Sequelize.STRING,
            trim: true
        },
        username: {
            type: Sequelize.STRING,
            trim: true
        },
        language_code: {
            type: Sequelize.STRING,
            trim: true
        },
        is_bot: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        },
        state: {
            type: Sequelize.STRING,
            defaultValue: 'start'
        },
    }, {
        freezeTableName: true,
    });
    return Users;
};